package aoc2016.day8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Day8A {
	private static boolean matrix[][];

	public static void main(String[] args) {
		initMatrix();

		String fileName = "src/main/resources/day8/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}
		countResult();

	}

	private static void countResult() {
		int counter = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if(matrix[i][j]) counter++;
				
			}
			
		}
		
		System.out.println(counter);
		
	}

	private static void processLine(String line) {
		if (line.contains("rect")) {
			String[] strings = line.split(" |x");
			doRect(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]));
		} else if (line.contains("rotate column")) {
			String[] strings = line.split(" |=");
			rotateColumn(Integer.parseInt(strings[3]), Integer.parseInt(strings[5]));
		} else if (line.contains("rotate row")) {
			String[] strings = line.split(" |=");
			rotateRow(Integer.parseInt(strings[3]), Integer.parseInt(strings[5]));
		}

		printMatrix();

	}

	private static void rotateRow(int a, int b) {
		boolean[] array = new boolean[matrix.length];

		for (int i = 0; i < matrix.length; i++) {
			array[i] = matrix[i][a];
		}
		boolean[] newArray = shift(array, b);

		for (int i = 0; i < matrix.length; i++) {
			matrix[i][a] = newArray[i];
		}
	}

	private static void rotateColumn(int a, int b) {
		boolean[] array = new boolean[matrix[a].length];

		for (int i = 0; i < matrix[a].length; i++) {
			array[i] = matrix[a][i];
		}

		boolean[] newArray = shift(array, b);

		for (int i = 0; i < matrix[a].length; i++) {
			matrix[a][i] = newArray[i];
		}
	}

	private static boolean[] shift(boolean[] array, int b) {
		boolean[] newArray = new boolean[array.length];

		for (int i = 0; i < array.length; i++) {
			newArray[(i + b) % array.length] = array[i];
		}

		return newArray;

	}

	private static void printMatrix() {
		System.out.println("-------------------------------");
		for (int i = 0; i < matrix[0].length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[j][i])
					System.out.print("#");
				else
					System.out.print(".");
			}
			System.out.println();
		}
		System.out.println("-------------------------------");

	}

	private static void doRect(int x, int y) {
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				matrix[i][j] = true;
			}
		}
	}

	private static void initMatrix() {
		matrix = new boolean[50][6];
	}
}
