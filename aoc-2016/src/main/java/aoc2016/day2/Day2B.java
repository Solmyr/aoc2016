package aoc2016.day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Day2B {
	private static Pad2 actualPosition = Pad2.FIVE;
	
	public static void main(String[] args) {
		String fileName = "src/main/resources/day2/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));
			

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void processLine(String line) {
		for (int i = 0; i < line.length(); i++) {
			actualPosition = actualPosition.nextForDirection(line.charAt(i));
		}
		System.out.print(actualPosition);
		
	}
}
