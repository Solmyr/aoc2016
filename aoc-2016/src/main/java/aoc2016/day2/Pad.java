package aoc2016.day2;

public enum Pad {
	ONE(1,2,4,1),
	TWO(2,3,5,1),
	THREE(3,3,6,2),
	FOUR(1,5,7,4),
	FIVE(2,6,8,4),
	SIX(3,6,9,5),
	SEVEN(4,8,7,7),
	EIGHT(5,9,8,7),
	NINE(6,9,9,8);
	
	private final int up;
	private final int right;
	private final int down;
	private final int left;
	
	private Pad(int up, int right, int down, int left) {
		this.up = up;
		this.right = right;
		this.down = down;
		this.left = left;
	}
	
	public Pad nextForDirection(char dir) {
		switch (dir) {
		case 'U': return Pad.values()[up - 1];
		case 'L': return Pad.values()[left - 1];
		case 'R': return Pad.values()[right - 1];
		case 'D': return Pad.values()[down - 1];
		}
		throw new IllegalArgumentException();
	}
	
	@Override
	public String toString() {
		return "" + (ordinal() + 1);
	}
	
	
}
