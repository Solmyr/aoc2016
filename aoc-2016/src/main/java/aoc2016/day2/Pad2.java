package aoc2016.day2;

public enum Pad2 {
	ONE("ONE","ONE","THREE","ONE"),
	TWO("TWO","THREE","SIX","TWO"),
	THREE("ONE","FOUR","SEVEN","TWO"),
	FOUR("FOUR","FOUR","EIGHT","THREE"),
	FIVE("FIVE","SIX","FIVE","FIVE"),
	SIX("TWO","SEVEN","A","FIVE"),
	SEVEN("THREE","EIGHT","B","SIX"),
	EIGHT("FOUR","NINE","C","SEVEN"),
	NINE("NINE","NINE","NINE","EIGHT"),
	A("SIX","B","A","A"),
	B("SEVEN","C","D","A"),
	C("EIGHT","C","C","B"),
	D("B","D","D","D");
	
	private final String up;
	private final String right;
	private final String down;
	private final String left;
	
	private Pad2(String up, String right, String down, String left) {
		this.up = up;
		this.right = right;
		this.down = down;
		this.left = left;
	}
	
	public Pad2 nextForDirection(char dir) {
		switch (dir) {
		case 'U': return Pad2.valueOf(up);
		case 'L': return Pad2.valueOf(left);
		case 'R': return Pad2.valueOf(right);
		case 'D': return Pad2.valueOf(down);
		}
		throw new IllegalArgumentException();
	}
	
	@Override
	public String toString() {
		if (ordinal() < 10) return "" + (ordinal() + 1);
		return super.toString();
	}
	
	
	
}
