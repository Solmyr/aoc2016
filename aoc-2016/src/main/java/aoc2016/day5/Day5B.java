package aoc2016.day5;

import java.util.Arrays;

import org.apache.commons.codec.digest.DigestUtils;

public class Day5B {
	public static void main(String[] args) {
		String in = "ojvtpuvg";
		//String in = "abc";
		int counter = 1;
		int i=0;
		char password[] = new char[8];
		for (int j = 0; j < password.length; j++) {
			password[i] = (char) 0;
		}
		
		
		while(true) {
			String fullHash = DigestUtils.md5Hex(in + counter);
			String hash = fullHash.substring(0, 5);
			counter++;
			
			if(hash.equals("00000")) {
				System.out.println("Nalezeno: " + hash + " pro: " + counter);
				char pos = fullHash.charAt(5);
				if(!Character.isDigit(pos)) continue;
				int intPos = Integer.parseInt("" + pos);
				if(intPos > 7) continue;
				if(password[intPos] != 0) continue;
				System.out.println(Arrays.toString(password));
			
				password[intPos] = fullHash.charAt(6);
				
				i++;
				if(i == 8) break;
			}			
			
			
			
		}
		System.out.println(Arrays.toString(password));
	}
}
