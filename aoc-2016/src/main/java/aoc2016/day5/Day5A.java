package aoc2016.day5;

import org.apache.commons.codec.digest.DigestUtils;

public class Day5A {
	public static void main(String[] args) {
		String in = "ojvtpuvg";
		int counter = 1;
		int i=0;
		String password = "";
		
		while(true) {
			String fullHash = DigestUtils.md5Hex(in + counter);
			String hash = fullHash.substring(0, 5);
			
			
			if(hash.equals("00000")) {
				password += "" + fullHash.charAt(5);
				i++;
				if(i == 8) break;
			}
			
			counter++;
			
		}
		System.out.println(password);
	}
}
