package aoc2016.day19;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.magicwerk.brownies.collections.GapList;

public class Day19A {
	public static void main(String[] args) {
		int input = 3014387;

		long startTime = System.currentTimeMillis();
		List<Integer> array = new GapList<>(input);

		for (int i = 0; i < input; i++) {
			array.add(i+1);
		}
		process(array);
		System.out.println("CAS: " +(System.currentTimeMillis() - startTime));
	}

	private static void process(List<Integer> array) {
		int index = 0;
		while (array.size() > 1) {
			int next = (index + 1) % array.size();
			array.remove(next);
			if (next > index)
				index++;
			index %= array.size();
		}
		System.out.println("--- " + array.get(0));

	}
}
