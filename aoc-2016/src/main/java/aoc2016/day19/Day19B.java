package aoc2016.day19;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.magicwerk.brownies.collections.GapList;

public class Day19B {
	public static void main(String[] args) {
		int input = 3014387;

		long startTime = System.currentTimeMillis();
		List<Integer> array = new GapList<>(input);
		process(array, input);
		System.out.println("GapList: " +(System.currentTimeMillis() - startTime));
	}

	
	private static void process(List<Integer> array, int input) {
		for (int i = 0; i < input; i++) {
			array.add(i+1);
		}
		
		int index = 0;
		while(array.size() > 1) {
			int next = (int)(array.size() / 2 + index) % array.size();
			array.remove(next);
			if(next > index) index++;
			index %= array.size();
		} 
		System.out.println(array.get(0));
		
	}
}
