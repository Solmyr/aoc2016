package aoc2016.day9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Day9A {
	public static void main(String[] args) {
		String fileName = "src/main/resources/day9/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> System.out.println(processPart(line)));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Long processPart(String part) {
		long finalLength = 0;
		while(part.length() > 0) {
			String[] subparts = part.split("\\(|x|\\)",4);
			if(subparts.length == 1) {
				finalLength += subparts[0].length();
				break;
			} else if(subparts.length > 1) {
				int cCount = Integer.parseInt(subparts[1]);
				int rCount = Integer.parseInt(subparts[2]);
				
				finalLength += subparts[0].length();
				finalLength += subparts[3].substring(0, cCount).length() * rCount;
				part = subparts[3].substring(cCount);	
			}
		}
		return finalLength;	
	}
}
