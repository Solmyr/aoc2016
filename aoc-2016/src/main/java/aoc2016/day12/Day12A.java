package aoc2016.day12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day12A {
	private static Map<String, Integer> registers = new HashMap<>();
	private static int pointer = 0;
	private static List<String> instructions;

	public static void main(String[] args) {

		String fileName = "src/main/resources/day12/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			instructions = stream.collect(Collectors.toList());
			run();
			System.out.println(registers);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void run() {
		while (pointer < instructions.size()) {
			String instruction = instructions.get(pointer);

			String[] parts = instruction.split(" ");
			switch (parts[0]) {
			case "cpy":
				doCpy(parts[1], parts[2]);
				pointer++;
				break;
			case "inc":
				doInc(parts[1]);
				pointer++;
				break;
			case "dec":
				doDec(parts[1]);
				pointer++;
				break;
			case "jnz":
				doJnz(parts[1],parts[2]);
				break;

			}

		}

	}

	private static void doDec(String a) {
		if(Character.isDigit(a.charAt(0))) throw new RuntimeException();
		registers.put(a, registerGet(a) - 1);
		
	}

	private static void doJnz(String a, String b) {
		int value = 0;
		if(Character.isAlphabetic(a.charAt(0))) {
			value = registerGet(a);
		} else {
			value = Integer.parseInt(a);
		}
		
		
		if(value != 0) {
			pointer += Integer.parseInt(b);
		} else {
			pointer++;
		}
		
	}

	private static int registerGet(String a) {

		if(Character.isDigit(a.charAt(0))) throw new RuntimeException();
		if(!registers.containsKey(a)) registers.put(a, 0);
		return registers.get(a);
	}

	private static void doInc(String a) {

		if(Character.isDigit(a.charAt(0))) throw new RuntimeException();
		registers.put(a, registerGet(a) + 1);

	}

	private static void doCpy(String a, String b) {

		if(Character.isDigit(b.charAt(0))) throw new RuntimeException();
		
		if(Character.isAlphabetic(a.charAt(0))) {
			int value = registerGet(a);
			registers.put(b, value);
		} else {
			registers.put(b, Integer.parseInt(a));
		}
	}
}
