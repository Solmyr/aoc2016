package aoc2016.day3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Day3B {
	private static int counter = 0;
	private static List<Integer> t1 = new ArrayList<>();
	private static List<Integer> t2 = new ArrayList<>();
	private static List<Integer> t3 = new ArrayList<>();
	
	public static void main(String[] args) {
		String fileName = "src/main/resources/day3/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(counter);
	}

	private static void processLine(String line) {
		String[] strings = line.split(" ");
		List<Integer> sides = new ArrayList<>();

		for (String string : strings) {
			if (string.equals(" "))
				continue;
			if (string.equals(""))
				continue;

			sides.add(Integer.parseInt(string));
		}

		t1.add(sides.get(0));
		t2.add(sides.get(1));
		t3.add(sides.get(2));

		if (t1.size() == 3) {
			proccesTriangle(t1);
			proccesTriangle(t2);
			proccesTriangle(t3);

			t1.clear();
			t2.clear();
			t3.clear();
		}
	}

	private static void proccesTriangle(List<Integer> sides) {
		Collections.sort(sides);
		if (sides.get(0) + sides.get(1) > sides.get(2))
			counter++;

	}

}
