package aoc2016.day4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Room {
	private Map<Character, Integer> characters = new HashMap<>();
	private String hash;
	private String number;
	private String realName = "";
	private int translate;
	
	public Room(String line) {
		
		
		String[] strings = line.split("-|\\[|\\]");
		number = strings[strings.length - 2];
		hash = strings[strings.length - 1];
		
		translate = Integer.parseInt(number);
		
		for (int i = 0; i < strings.length-2; i++) {
			addToStats(strings[i]);
			addToReal(strings[i]);
		}
		
	}
	
	private void addToReal(String string) {
		string = string.toLowerCase();
		
		
		for (int i = 0; i < string.length(); i++) {
			int start = 'a';
			int end =  'z' - start + 1;
			int t = string.charAt(i) - start + translate;
			t = t % end;
			t = t + start; 
			char newChar = (char) t; 
			
			realName += "" + newChar;
		}
		
		
		realName += " ";
	}

	private void addToStats(String string) {
		for (char c : string.toCharArray()) {
			if(!characters.containsKey(c)) characters.put(c, 0);
			
			int count = characters.get(c);
			count++;
			characters.put(c, count);
		}
	}




	@Override
	public String toString() {
		return "Room [characters=" + characters + ", hash=" + hash + ", number=" + number + "]";
	}
	
	public boolean isReal() {
		String countedHash = countHash();
		return hash.equals(countedHash);
	}

	private String countHash() {
		ArrayList<Entry<Character, Integer>> list = new ArrayList<>(characters.entrySet());
		
		list.sort(new Comparator<Entry<Character, Integer>>() {
			@Override
			public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
				int result = Integer.compare(o2.getValue(), o1.getValue());
				if(result != 0) return result;
				return Character.compare(o1.getKey(), o2.getKey());
			}	
		});
		
		String countedHash = "";
		
		for(int i = 0; i < 5 && i < list.size(); i++) {
			countedHash += list.get(i).getKey();
		}
		
		return countedHash;
	}

	public Map<Character, Integer> getCharacters() {
		return characters;
	}

	public String getHash() {
		return hash;
	}

	public String getNumber() {
		return number;
	}

	public String getRealName() {
		return realName;
	}

	

	
}
