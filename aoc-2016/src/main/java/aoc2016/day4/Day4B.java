package aoc2016.day4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Day4B {
	public static void main(String[] args) {
		String fileName = "src/main/resources/day4/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	private static void processLine(String line) {
		Room r = new Room(line);
		if(r.getRealName().contains("north")) {
			System.out.println(r.getRealName());
			System.out.println(r.getNumber());
		}
		
		
	}
}
