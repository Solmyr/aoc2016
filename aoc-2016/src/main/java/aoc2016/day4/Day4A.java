package aoc2016.day4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Day4A {
	private static int counter = 0;
	public static void main(String[] args) {
		String fileName = "src/main/resources/day4/inTest2";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(counter);
	}

	private static void processLine(String line) {
		Room r = new Room(line);
		if(r.isReal()) counter += Integer.parseInt(r.getNumber());
	}
}
