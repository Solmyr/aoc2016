package aoc2016.day7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Day7B {
	private static int counter = 0;

	public static void main(String[] args) {
		String fileName = "src/main/resources/day7/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(counter);

	}

	private static void processLine(String line) {
		List<String> ibab = getInvertedBAB(line);
		List<String> aba = getABA(line);
		int originalSize = aba.size();
		aba.removeAll(ibab);
		if(aba.size() != originalSize)  {
			counter++;
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		}
		
		
		
	}

	private static List<String> getABA(String line) {
		String[] s = line.split("\\[.*?\\]");
		return getXYX(new ArrayList<>(Arrays.asList(s)));
	}

	private static List<String> getInvertedBAB(String line) {
		List<String> innners = getBracketsIN(line);
		
		List<String> babs = getXYX(innners);
		List<String> ibabs = new ArrayList<>();
		
		for (String bab : babs) {
			ibabs.add(bab.substring(1) + bab.charAt(1));
		}
		
		return ibabs;
	}

	private static List<String> getXYX(List<String> innners) {
		List<String> xyxs = new ArrayList<>();
		
		for (String string : innners) {
			for (int i = 0; i < string.length() - 2; i++) {
				char c1 = string.charAt(i);
				char c2 = string.charAt(i + 1);
				char c3 = string.charAt(i + 2);

				if (c1 == c2)
					continue;
				if (c1 == c3) {
					xyxs.add("" + c1 + c2 + c3);
				}
			}
		}
		
		return xyxs;
		
		
	}

	private static List<String> getBracketsIN(String line) {
		Pattern p = Pattern.compile("\\[(.*?)\\]");
		Matcher m = p.matcher(line);

		List<String> strings = new ArrayList<>();
		
		while(m.find()) {
		  strings.add(m.group(1));
		}
		
		return strings;
	}
}
