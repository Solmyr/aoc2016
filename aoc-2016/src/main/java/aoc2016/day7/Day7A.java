package aoc2016.day7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Day7A {
	private static int counter = 0;

	public static void main(String[] args) {
		String fileName = "src/main/resources/day7/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(counter);

	}

	private static void processLine(String line) {
		String[] s = line.split("\\[.*?\\]");

		Pattern p = Pattern.compile("\\[(.*?)\\]");
		Matcher m = p.matcher(line);

		while(m.find()) {
		   if(checkString(m.group(1))) return;
		}
		
		for (int i = 0; i < s.length; i++) {
			if(checkString(s[i])) {
				counter++;
				return;
			}	
		}
	}

	private static boolean checkString(String s) {

		for (int i = 0; i < s.length() - 3; i++) {
			char c1 = s.charAt(i);
			char c2 = s.charAt(i + 1);
			char c3 = s.charAt(i + 2);
			char c4 = s.charAt(i + 3);

			if (c1 == c2)
				continue;
			if (c1 == c4 && c2 == c3) {
				return true;
			}
		}

		return false;
	}
}
