package aoc2016.day11;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day11B {
	private static LinkedList<State> lifo = new LinkedList<>();
	private static Set<State> allStates = new HashSet<>();

	public static void main(String[] args) {
		State start = startState();
		lifo.addLast(start);
		allStates.add(start);
		int totalCounter = 0;

		while (!lifo.isEmpty()) {
			State actualState = lifo.removeFirst();
			if(actualState.getId() % 1000 == 0) System.out.println("Vytvoreno ID = " + actualState.getId() + " L: " + actualState.getLayer() + " ALL: " + allStates.size());
			//System.out.println("========================== Zpracovavam =====================");
			//print(actualState);
			generateNew(actualState);
			totalCounter++;
			
			if (totalCounter == 2) {
				//System.exit(0);
			}
		}
		
		System.out.println("DOSLO");

	}

	private static void generateNew(State actualState) {
		int actualElevator = actualState.getElevator();

		if (actualState.getElevator() > 0) {
			for (String module : actualState.getFloors().get(actualElevator)) {
				State childState = actualState.createChild();
				childState.getFloors().get(actualElevator).remove(module);
				childState.getFloors().get(actualElevator - 1).add(module);
				childState.setElevator(actualElevator - 1);

				if (checks(childState)) {
					continue;
				}

				lifo.addLast(childState);
				allStates.add(childState);

			}
			/*
			for (int i = 0; i < actualState.getFloors().get(actualElevator).size(); i++) {
				String s1 = actualState.getFloors().get(actualElevator).get(i);
				for (int j = i + 1; j < actualState.getFloors().get(actualElevator).size(); j++) {
					String s2 = actualState.getFloors().get(actualElevator).get(j);
					
					State childState = actualState.createChild();
					childState.getFloors().get(actualElevator).remove(s1);
					childState.getFloors().get(actualElevator).remove(s2);
					childState.getFloors().get(actualElevator - 1).add(s1);
					childState.getFloors().get(actualElevator - 1).add(s2);
					childState.setElevator(actualElevator - 1);

					if (checks(childState)) {
						continue;
					}

					lifo.addLast(childState);
					allStates.add(childState);
					
				}
				
			}*/
			
			
		}

		if (actualState.getElevator() + 1 < actualState.getFloors().size()) {
			/*
			for (String module : actualState.getFloors().get(actualElevator)) {
				State childState = actualState.createChild();
				childState.getFloors().get(actualElevator).remove(module);
				childState.getFloors().get(actualElevator + 1).add(module);
				childState.setElevator(actualElevator + 1);

				if (checks(childState)) {
					continue;
				}

				lifo.addLast(childState);
				allStates.add(childState);
			}
			*/
			for (int i = 0; i < actualState.getFloors().get(actualElevator).size(); i++) {
				String s1 = actualState.getFloors().get(actualElevator).get(i);
				for (int j = i + 1; j < actualState.getFloors().get(actualElevator).size(); j++) {
					String s2 = actualState.getFloors().get(actualElevator).get(j);
					
					State childState = actualState.createChild();
					childState.getFloors().get(actualElevator).remove(s1);
					childState.getFloors().get(actualElevator).remove(s2);
					childState.getFloors().get(actualElevator + 1).add(s1);
					childState.getFloors().get(actualElevator + 1).add(s2);
					childState.setElevator(actualElevator + 1);
				
					if (checks(childState)) {
						continue;
					}
					
					lifo.addLast(childState);
					allStates.add(childState);
					
				}
				
			}
		}

	}

	private static boolean checks(State state) {
		//System.out.println("==== checkuju ====");
		//print(state);
		
		if (checkRepetition(state)){
			//System.out.println("Opakovani\n\n");
			return true;
			
		}
			if (checkLogic(state)){
				//System.out.println("Fried\n\n");
				return true;
			}
			
		if(checkWin(state)) {
			System.out.println("\n\n\n*******  RESENI *******\n\n");
			List<State> path = new ArrayList<>();
			
			State t = state;
			
			while(t != null) {
				path.add(t);
				t = t.getPrev();
			}
			
			System.out.println(path.size() - 1);
			
			
			System.exit(0);
		}

		//System.out.println("OK\n\n");
		return false;
	}

	private static boolean checkWin(State state) {
		if(state.getFloors().get(0).size() != 0) return false;
		if(state.getFloors().get(1).size() != 0) return false;
		if(state.getFloors().get(2).size() != 0) return false;
		return true;
	}

	private static boolean checkLogic(State state) {
		for (List<String> floor : state.getFloors()) {
			Set<String> chips = floor.stream().filter(e -> e.charAt(1) == 'M').collect(Collectors.toSet());
			Set<String> generators = floor.stream().filter(e -> e.charAt(1) == 'G').collect(Collectors.toSet());
			if (chips.isEmpty() || generators.isEmpty()) {
				continue;
			} else {
				for (String ch : chips) {
					if (!floor.contains(ch.charAt(0) + "G")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private static boolean checkRepetition(State childState) {
		if (allStates.contains(childState))
			return true;

		return false;
	}

	private static void printNew(State actualState, State childState) {
		System.out.println("=====  Novy   =====");
		print(childState);
		System.out.println("===================\n\n\n");

	}

	private static void print(State state) {
		System.out.println("ID : " + state.getId() + " L: " + state.getLayer());
		System.out.println("Elevator = " + state.getElevator());
		System.out.println(state.getFloors().get(3));
		System.out.println(state.getFloors().get(2));
		System.out.println(state.getFloors().get(1));
		System.out.println(state.getFloors().get(0));
		System.out.println(state.hashCode());

	}

	private static State startState() {
		State startState = new State();

		for (int i = 0; i < 4; i++)
			startState.getFloors().add(new ArrayList<>());

		startState.getFloors().get(0).add("SG");
		startState.getFloors().get(0).add("SM");
		startState.getFloors().get(0).add("PG");
		startState.getFloors().get(0).add("PM");
		startState.getFloors().get(0).add("EG");
		startState.getFloors().get(0).add("EM");
		startState.getFloors().get(0).add("DG");
		startState.getFloors().get(0).add("DM");
		
		
		
		startState.getFloors().get(1).add("TG");
		startState.getFloors().get(1).add("RG");
		startState.getFloors().get(1).add("RM");
		startState.getFloors().get(1).add("CG");
		startState.getFloors().get(1).add("CM");
		startState.getFloors().get(2).add("TM");

		startState.setElevator(0);
		startState.setLayer(0);
		return startState;
	}
	
	private static State startTestState() {
		State startState = new State();

		for (int i = 0; i < 4; i++)
			startState.getFloors().add(new ArrayList<>());

		startState.getFloors().get(0).add("HM");
		startState.getFloors().get(0).add("LM");
		startState.getFloors().get(1).add("HG");
		startState.getFloors().get(2).add("LG");

		startState.setElevator(0);
		startState.setLayer(0);
		return startState;
	}

}
