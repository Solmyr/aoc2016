package aoc2016.day11;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class State  {
	
	private static int counter = 0;
	private List<List<String>> floors;
	private int elevator;
	private State prev;
	private int layer;
	
	
	
	private int id;

	public State() {
		floors = new ArrayList<List<String>>();
		id = counter;
		counter++;
		
		
	}
	
	
	




	public State createChild()  {
		State clone = new State();
		
		for (List<String> list : floors) {
			clone.getFloors().add(new ArrayList<>(list));
		}
		
		clone.elevator = elevator;
		clone.prev = this;
		clone.layer = layer + 1;
		
		return clone;
	}




	public int getElevator() {
		return elevator;
	}
	
	
	
	
	public List<List<String>> getFloors() {
		return floors;
	}




	public int getId() {
		return id;
	}




	public int getLayer() {
		return layer;
	}



	public State getPrev() {
		return prev;
	}



	@Override
	public int hashCode() {
		String s = "" + elevator + ";";
		for (List<String> list : floors) {
			List<String> sorted = new ArrayList<>(list);
			s+="x";
			Collections.sort(sorted, Collator.getInstance());
			for (String string : sorted) {
				s += string;
			}
			
		}
		
		return s.hashCode();
	}



	public void setElevator(int elevator) {
		this.elevator = elevator;
	}



	public void setFloors(List<List<String>> floors) {
		this.floors = floors;
	}
	
	public void setLayer(int layer) {
		this.layer = layer;
	}
	
	public void setPrev(State prev) {
		this.prev = prev;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof State)) return false;
		
		return ((State)obj).hashCode() == hashCode();
	}
	
	
	
	
}
