package aoc2016.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Day1B {
	
	private static Set<String> points = new HashSet<String>();
	
	public static void main(String[] args) {
		String fileName = "path";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			String line = stream.findFirst().get();
			processLine(line);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void processLine(String line) {
		String[] commands = line.split(", ");
		System.out.println(Arrays.toString(commands));

		int x = 0;
		int y = 0;
		int dir = 0;
		for (String command : commands) {
			
			if (command.charAt(0) == 'R')
				dir++;
			else {
				dir--;
				dir += 4;
			}
			dir %= 4;

			int steps = Integer.parseInt("" + command.substring(1));

			switch (dir) {
			case 0:
				for (int i = 0; i < steps; i++) {
					x++;
					check(x,y);
				}
				break;
			case 1:
				for (int i = 0; i < steps; i++) {
					y++;
					check(x,y);
				}
				break;
			case 2:
				for (int i = 0; i < steps; i++) {
					x--;
					check(x,y);
				}
				break;
			case 3:
				for (int i = 0; i < steps; i++) {
					y--;
					check(x,y);
				}
				break;
			}
			System.out.println("Command: " + command + " X: " + x + " Y: " + y);

		}

	}

	private static void check(int x, int y) {
		String point = "" + x + "-" + y;
		if (points.contains(point)) {
			System.out.println("KONCIM");
			System.out.println("Vysledek: " + (Math.abs(x) + Math.abs(y)));
			System.exit(0);
		}
		points.add(point);
		
	}
}
