package aoc2016.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class Day1 {
	public static void main(String[] args) {
		String fileName = "src/main/resources/day1/path";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			String line = stream.findFirst().get();
			processLine(line);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void processLine(String line) {
		String[] commands = line.split(", ");
		System.out.println(Arrays.toString(commands));

		int x = 0;
		int y = 0;
		int dir = 0;
		for (String command : commands) {
			if (command.charAt(0) == 'R')
				dir++;
			else {
				dir--;
				dir += 4;
			}
			dir %= 4;

			int steps = Integer.parseInt("" + command.substring(1));

			switch (dir) {
			case 0:
				x += steps;
				break;
			case 1:
				y += steps;
				break;
			case 2:
				x -= steps;
				break;
			case 3:
				y -= steps;
				break;
			}
			System.out.println("Command: " + command + " X: " + x + " Y: " + y);

		}
		
		System.out.println("Vysledek: " + (Math.abs(x) + Math.abs(y)));

	}
}
