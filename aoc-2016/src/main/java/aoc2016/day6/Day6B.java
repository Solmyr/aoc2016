package aoc2016.day6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Day6B {
	private static List<Map<Character, Integer>> list;
	
	public static void main(String[] args) {
		String fileName = "src/main/resources/day6/in";

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(line -> processLine(line));

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		printResult();
		
		
	}

	private static void printResult() {
		for (Map<Character, Integer> map : list) {
			System.out.print(
					map
						.entrySet()
						.stream()
						.min((e1,e2) -> Integer.compare(e1.getValue(), e2.getValue()))
						.get().getKey()
			);
		}
		
	}

	private static void processLine(String line) {
		System.out.println("Proccess " + line);
		if(list == null) {
			list = new ArrayList<>();
			for (int i = 0; i < line.length(); i++) {
				list.add(new HashMap<>());
			}
		}
			
		
		for (int i = 0; i < line.length(); i++) {
			Map<Character, Integer> map = list.get(i);
			char c = line.charAt(i);
			
			if(!map.containsKey(c)) map.put(c, 0);
			map.put(c, map.get(c) + 1);
		}
	}
}
