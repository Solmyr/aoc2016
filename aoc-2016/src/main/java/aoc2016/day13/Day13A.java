package aoc2016.day13;


public class Day13A {
	
	public static void main(String[] args) {

		//boolean[][] maze = generateMaze(10, 10, 7);
		boolean[][] maze = generateMaze(1352, 100, 100);
		
		Resolver r = new Resolver();
		//r.resolveMaze(maze, 7, 4);
		r.resolveMaze(maze, 31, 39);
		
	}	

	private static boolean[][] generateMaze(int seed, int sizeX, int sizeY) {
		boolean maze[][] = new boolean[sizeY][sizeX];
		for (int x = 0; x < maze[0].length; x++) {
			for (int y = 0; y < maze.length; y++) {
				int temp = x * x + 3 * x + 2 * x * y + y + y * y;
				temp += seed;
				String binary = Integer.toBinaryString(temp);
				boolean isEven = true;
				for (int i = 0; i < binary.length(); i++) {
					if (binary.charAt(i) == '1')
						isEven = !isEven;
				}
				maze[y][x] = !isEven;
			}
		}

		printMaze(maze);
		return maze;

	}

	private static void printMaze(boolean[][] maze) {
		for (int y = 0; y < maze.length; y++) {
			for (int x = 0; x < maze[0].length; x++) {
				if (maze[y][x])
					System.out.print("#");
				else
					System.out.print(".");
			}
			System.out.println();
		}
	}
	
	
}
