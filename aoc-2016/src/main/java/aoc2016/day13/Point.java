package aoc2016.day13;

public class Point {
	public int x;
	public int y;
	public Point prev;
	public Point(int x, int y, Point prev) {
		super();
		this.x = x;
		this.y = y;
		this.prev = prev;
	}
	
	@Override
	public String toString() {
		return "P( X = " + x + ", Y = " + y + " ) " + (prev != null ? "PREV: " + prev : "");
	}
	
	public int getPathLen() {
		Point temp = this;
		int counter = -1;
		while (temp != null) {
			temp = temp.prev;
			counter++;
		}
		return counter;
	}
}
