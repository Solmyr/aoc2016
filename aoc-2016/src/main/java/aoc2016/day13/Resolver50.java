package aoc2016.day13;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Resolver50 {

	private LinkedList<Point> lifo = new LinkedList<>();
	private Set<String> allpoints = new HashSet<>();

	public void resolveMaze(boolean[][] maze, int targetX, int targetY) {
		lifo.addLast(new Point(1, 1, null));

		int coutner = 0;
		while (!lifo.isEmpty()) {
			Point actual = lifo.removeFirst();
			
			coutner++;
			System.out.println(allpoints.size());

			if (actual.x == targetX && actual.y == targetY) {
				System.out.println("Nalezeno reseni " + actual);
				printSolution(actual, maze);
				break;
			}
			if(coutner%1000 ==0) System.out.println("Counter: " + coutner);
			genNew(actual, maze);
		}

	}

	private void genNew(Point actual, boolean[][] maze) {
		int x = actual.x;
		int y = actual.y;

		if (x > 0 && !maze[y][x - 1]) {
			Point p = new Point(x - 1, y, actual);
			registerPoint(p);
		}
		if (y > 0 && !maze[y - 1][x]) {
			Point p = new Point(x, y - 1, actual);
			registerPoint(p);
		}
		if (x + 1 < maze[0].length && !maze[y][x + 1]) {
			Point p = new Point(x + 1, y, actual);
			registerPoint(p);
		}
		if (y + 1 < maze.length && !maze[y + 1][x]) {
			Point p = new Point(x, y + 1, actual);
			registerPoint(p);
		}
	}

	private void registerPoint(Point p) {
		String ps = p.x + "-" + p.y;
		if (allpoints.contains(ps))
			return;
		lifo.add(p);
		allpoints.add(ps);
		
	}

	private void printSolution(Point actual, boolean[][] blankmaze) {
		char maze[][] = new char[blankmaze.length][blankmaze[0].length];

		for (int y = 0; y < maze.length; y++) {
			for (int x = 0; x < maze[0].length; x++) {
				maze[y][x] = blankmaze[y][x] ? '#' : '.';
			}
		}

		Point temp = actual;
		int counter = -1;
		while (temp != null) {
			maze[temp.y][temp.x] = 'O';
			temp = temp.prev;
			counter++;
		}

		for (int y = 0; y < maze.length; y++) {
			for (int x = 0; x < maze[0].length; x++) {

				System.out.print(maze[y][x]);
			}
			System.out.println();
		}
		System.out.println("Delka: " + counter);

	}

}
