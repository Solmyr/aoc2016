package aoc2016.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Day10A {
	private static Map<Integer, Bot> bots = new HashMap<>();
	private static Map<Integer, List<Integer>> bins = new HashMap<>();

	public static void main(String[] args) {
		String fileName = "src/main/resources/day10/in";

		try {
			Stream<String> stream = Files.lines(Paths.get(fileName));
			stream.forEach(line -> processLine1(line));
			stream.close();
			stream = Files.lines(Paths.get(fileName));
			stream.forEach(line -> processLine2(line));

		} catch (IOException e) {
			e.printStackTrace();
		}

		printResults();

	}

	private static void printResults() {
		System.out.println("----   Bins   ----");

		bins.entrySet().stream().forEach(e -> System.out.println((e.getKey() - 1) + " - " + e.getValue()));

		System.out.println();

	}

	private static void processLine1(String line) {
		System.out.println("1. Zpracovavam " + line);

		String[] parts = line.split(" ");
		System.out.println(Arrays.toString(parts));
		if (parts[0].equals("bot")) {
			setRulesToBot(Integer.parseInt(parts[1]), parts[5], Integer.parseInt(parts[6]), parts[10],
					Integer.parseInt(parts[11]));
		} 
	}
	
	private static void processLine2(String line) {
		System.out.println("Zpracovavam " + line);

		String[] parts = line.split(" ");
		System.out.println(Arrays.toString(parts));
		if (parts[0].equals("value")) {
			setValueToBot(Integer.parseInt(parts[1]), Integer.parseInt(parts[5]));
		} 
	}

	private static void setRulesToBot(int botId, String action1, int value1, String action2, int value2) {
		Bot bot = getBot(botId);

		if (action1.equals("bot")) {
			bot.setRuleLow(value1);
		} else {
			bot.setRuleLow(-(value1 + 1));
		}

		if (action2.equals("bot")) {
			bot.setRuleHigh(value2);
		} else {
			bot.setRuleHigh(-(value2 + 1));
		}

	}

	private static void setValueToBot(int value, int botId) {
		if (botId < 0) {
			if (!bins.containsKey(-botId))
				bins.put(-botId, new ArrayList<>());
			bins.get(-botId).add(value);
		}

		Bot bot = getBot(botId);

		if (bot.getChip1() == null) {
			bot.setChip1(value);
		} else {
			bot.setChip2(value);
			resolveBot(bot, botId);
		}

	}

	private static void resolveBot(Bot bot, int botId) {
		System.out.println("Resolvuju " + botId);
		int value1 = bot.getChip1();
		int value2 = bot.getChip2();

		if (bot.getRuleHigh() == null || bot.getRuleLow() == null) {
			throw new RuntimeException();
		}

		if ((bot.getChip1() == 61 && bot.getChip2() == 17) || (bot.getChip1() == 17 && bot.getChip2() == 61)) {
			System.out.println("BOT " + botId + " porovnava 61 a 17");
		}

		bot.setChip1(null);
		bot.setChip2(null);

		if (value1 < value2) {
			setValueToBot(value1, bot.getRuleLow());
			setValueToBot(value2, bot.getRuleHigh());
		} else {
			setValueToBot(value1, bot.getRuleHigh());
			setValueToBot(value2, bot.getRuleLow());
		}

	}

	private static Bot getBot(int bot) {
		if (!bots.containsKey(bot)) {
			bots.put(bot, new Bot());
		}
		return bots.get(bot);
	}
}
