package aoc2016.day10;

public class Bot {
	private Integer ruleLow;
	private Integer ruleHigh;
	private Integer chip1;
	private Integer chip2;
	public Integer getChip1() {
		return chip1;
	}
	public Integer getChip2() {
		return chip2;
	}
	public Integer getRuleHigh() {
		return ruleHigh;
	}
	public Integer getRuleLow() {
		return ruleLow;
	}
	public void setChip1(Integer chip1) {
		this.chip1 = chip1;
	}
	public void setChip2(Integer chip2) {
		this.chip2 = chip2;
	}
	
	public void setRuleHigh(Integer ruleHigh) {
		this.ruleHigh = ruleHigh;
	}
	public void setRuleLow(Integer ruleLow) {
		this.ruleLow = ruleLow;
	}

}
